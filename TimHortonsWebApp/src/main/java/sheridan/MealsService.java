package sheridan;

import sheridan.MealType;

import java.util.ArrayList;
import java.util.List;

public class MealsService {
	

    public static List<String> getAvailableMealTypes( MealType type ){

        List<String> types = new ArrayList<String>( );

        if( type != null && type.equals( MealType.DRINKS ) ){
//          types.add( "TBD" );
//          types.add( "TBD" );
        	types.add("Coffee");
        	types.add("Ice Capp");
        	types.add("Tea");
        	types.add("Water");
        }
        else if( type != null && type.equals( MealType.BAKEDGOODS ) ){
//            types.add( "TBD" );
//            types.add( "TBD" );
        }
        else {
        	types.add("No Brand Available");
        }
        return types;
    }
}
