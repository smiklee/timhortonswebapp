package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDrinksRegular() {
		assertTrue("Drinks list is not empty", MealsService.getAvailableMealTypes(MealType.DRINKS) != null);
	}
	
	@Test
	public void testDrinksException() {
		assertTrue("First element is correct", MealsService.getAvailableMealTypes(null).get(0).equals("No Brand Available"));
	}
	
	@Test
	public void testDrinksBoundaryIn() {
		assertTrue("The number of elements is correct", MealsService.getAvailableMealTypes(MealType.DRINKS).size() > 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		assertTrue("The number of elements is correct", MealsService.getAvailableMealTypes(null).size() <= 1);
	}

}
